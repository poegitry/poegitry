# *poegitry* (poetry in git)

Welcome to *poegitry* project!\
Bienvenue dans le projet *poegitry* !\
¡Bienvenido al proyecto *poegitry*!

## Quick how to

Choose a branch to access the text of a play.\
Choisissez une branche pour accéder au texte d'une pièce de théâtre.\
Elige una rama para acceder al texto de una obra de teatro.

## Credits

*poegitry* is an art work by Anthony Baillard.\
*poegitry* est une œuvre d'art d'Anthony Baillard.\
*poegitry* es una obra de arte de Anthony Baillard.

Character designs / Dessins des personnages / Diseños de los personajes:
- The Tempest: Adèle Jelansky <a href="https://www.instagram.com/jelansky_/" target="_blank">@jelansky</a>
- Le Pauvre Bougre et le Bon Génie: Franck Gallio <a href="https://www.flickr.com/people/franckgallio/" target="_blank">@franckgallio</a>

## What is *poegitry*

<a href="https://git-scm.com/" target="_blank">*git*</a> is a version control system widely used by the international coder community. It is mainly use to handle source code in a collaborative way: *branches*, *commits*, *contributors*, are part of the common vocabulary of *git* and refer to technical concepts.

## Qu'est *poegitry*

<a href="https://git-scm.com/" target="_blank">*git*</a> est un système de contrôle de version largemnt utilisé par la communauté internationale de développeurs. Il est principalement utilisé pour gérer le code source de façon collaborative : *branches*, *soumissions/commits*, *contributeurs* font partie du vocabulaire commun de *git* et se réfèrent à des concepts techniques.

*poegitry* fait un tout autre usage de l'outil. Les branches deviennent des pièces de théatre, les commits sont les répliques et les contributeurs sont les personnages. La puissance de *git* est ici mise à profit pour proposer un nouveau mode de lecture.

*poegitry* est également une œuvre plastique, chaque pièce étant choisie et illustrée par un artiste différent, y ajoutant ainsi son esthétisme et sa sensibilité.

